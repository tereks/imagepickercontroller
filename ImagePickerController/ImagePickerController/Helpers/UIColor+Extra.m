//
//  UIColor+Extra.m
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "UIColor+Extra.h"

@implementation UIColor (Extra)

+ (UIColor *)colorFromHexString:(NSString *)string {
    int red = 0;
    int green = 0;
    int blue = 0;
    sscanf([string UTF8String], "#%02X%02X%02X", &red, &green, &blue);
    return  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.f];
}

+ (UIColor* ) flamingoColor {
    return [UIColor colorFromHexString:@"#F05A29"];
}

@end
