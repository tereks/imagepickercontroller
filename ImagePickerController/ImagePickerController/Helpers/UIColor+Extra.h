//
//  UIColor+Extra.h
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extra)

+ (UIColor *) colorFromHexString:(NSString *)string;
+ (UIColor *) flamingoColor;

@end
