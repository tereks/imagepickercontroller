//
//  TitleHelper.m
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "TitleHelper.h"

@implementation TitleHelper

+ (NSString*) nameWithCount:(NSUInteger)count {
    if ( count == 1 ) {
        return [NSString stringWithFormat:@"Attach %lu file", (unsigned long)count];  
    }
    return [NSString stringWithFormat:@"Attach %lu files", (unsigned long)count];  
}

@end
