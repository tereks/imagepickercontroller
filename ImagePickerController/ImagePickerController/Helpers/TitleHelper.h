//
//  TitleHelper.h
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TitleHelper : NSObject

+ (NSString*) nameWithCount:(NSUInteger)count;

@end
