//
//  ActionSheetCollectionLayout.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ActionSheetCollectionLayout.h"

@interface ActionSheetCollectionLayout ()

@property (nonatomic, strong) NSMutableArray<NSMutableArray*>* layoutAttributes;
@property (nonatomic, strong) NSMutableArray* invalidatedLayoutAttributes;
@property (nonatomic, assign) CGSize contentSize;

@end

@implementation ActionSheetCollectionLayout

- (instancetype) init {
    self = [super init];
    
    if ( self ) {
        self.layoutAttributes            = [NSMutableArray new];
        self.invalidatedLayoutAttributes = [NSMutableArray new];
        self.contentSize                 = CGSizeZero;
    }
    
    return self;
}

- (void) prepareLayout {
    [super prepareLayout];
    
    [self.layoutAttributes removeAllObjects];
    self.contentSize = CGSizeZero;
    
    if ( self.collectionView && self.collectionView.dataSource && self.collectionView.delegate ) {
        
        NSInteger sectionCount = [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView];
        CGPoint origin = CGPointZero;
        
        for ( NSInteger section = 0; section < sectionCount; section++ ) {
            NSMutableArray<UICollectionViewLayoutAttributes *> *sectionAttributes = [[NSMutableArray alloc] init];
            
            NSInteger itemsCount = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:section];
            
            NSMutableArray *indexPaths = [NSMutableArray new];
            for ( NSInteger item = 0; item < itemsCount; item++ ) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:item inSection:section]];
            }
            
            id <UICollectionViewDelegateFlowLayout> delegate = (id <UICollectionViewDelegateFlowLayout>)self.collectionView.delegate;
            for ( NSIndexPath *indexPath in indexPaths ) {
                CGSize size = [delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
                                
                UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath: indexPath];
                attributes.frame = CGRectMake( origin.x, origin.y, size.width, size.height);
                
                [sectionAttributes addObject:attributes];
                origin.y = CGRectGetMaxY( attributes.frame );
            }
            
            [self.layoutAttributes addObject:sectionAttributes];
        }
        
        self.contentSize = CGSizeMake( CGRectGetWidth (self.collectionView.frame), origin.y);
    }
}

- (BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (void) invalidateLayout {
    self.invalidatedLayoutAttributes = self.layoutAttributes;
    [super invalidateLayout];
}

- (CGSize) collectionViewContentSize {
    return self.contentSize;
}

- (nullable NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {    
    
    NSMutableArray * longArray = [NSMutableArray new];
    for (NSArray * attributes in self.layoutAttributes) {
        [longArray addObjectsFromArray:attributes];
    }
    
    NSPredicate *intersects = [NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes* obj, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, obj.frame);
    }];
    return [longArray filteredArrayUsingPredicate:intersects];
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForItemAtIndexPath:(NSIndexPath*)indexPath allAttributes:(NSArray<NSArray*>*)allAttributes {
    
    if ( !(allAttributes.count > indexPath.section && allAttributes[indexPath.section].count > indexPath.item) ) {
        return nil;
    }
    
    return allAttributes[indexPath.section][indexPath.item];
}

- (UICollectionViewLayoutAttributes *) invalidatedLayoutAttributesForItemAtIndexPath:(NSIndexPath*)indexPath {
    if ( !self.invalidatedLayoutAttributes ) {
        return nil;
    }
    
    return [self layoutAttributesForItemAtIndexPath:indexPath allAttributes:self.invalidatedLayoutAttributes];
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    return [self layoutAttributesForItemAtIndexPath:itemIndexPath allAttributes:self.layoutAttributes];
}

- (UICollectionViewLayoutAttributes *) initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    UICollectionViewLayoutAttributes * invAttrs = [self invalidatedLayoutAttributesForItemAtIndexPath:itemIndexPath];
    if ( !invAttrs ) {
        return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    }
    return invAttrs;
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
}

@end
