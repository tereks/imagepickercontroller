//
//  ActionSheetController.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIkit.h>
#import "PickerActionsDataSource.h"
#import "SheetDataModel.h"

#import "ActionSheetCell.h"
#import "PreviewCell.h"

#import "ImagePickerAction.h"
#import "Constants.h"

#import "ImagesPreviewController.h"

@protocol ActionSheetControllerDelegate <NSObject>

- (void) actionSelected:(ImagePickerAction*)action;

@end 

@interface ActionSheetController : NSObject

@property (nonatomic, readonly, strong) UICollectionView * sheetCollectionView;
@property (nonatomic, readonly, strong) ImagesPreviewController * imagePreviewController;

@property (nonatomic, strong) PickerActionsDataSource *dataSource;
@property (nonatomic, strong) SheetDataModel * dataModel;

@property (nonatomic, assign) CGFloat preferredSheetWidth;
@property (nonatomic, assign) CGFloat preferredSheetHeight;

@property (nonatomic, strong) NSIndexPath * cancelIndexPath;

@property (nonatomic, weak) id<ActionSheetControllerDelegate> delegate;

@end
