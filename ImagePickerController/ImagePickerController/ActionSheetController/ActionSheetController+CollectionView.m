//
//  ActionSheetController+CollectionView.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ActionSheetController+CollectionView.h"
#import "UIColor+Extra.h"

@implementation ActionSheetController (CollectionView)

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ( section == 0 ) {
        return 1;
    }
    return self.dataSource.actions.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BaseCollectionCell * cell;
    
    if ( indexPath.section == 0 ) {
        PreviewCell * previewCell = [collectionView 
                                     dequeueReusableCellWithReuseIdentifier:kPreviewCellReuseIdentifier 
                                     forIndexPath:indexPath];
        
        previewCell.collectionView = self.imagePreviewController.previewCollectionView;
        cell = previewCell;
    }
    else {
        ImagePickerAction * action = self.dataSource.actions[indexPath.row];
        
        ActionSheetCell* actionCell = [collectionView dequeueReusableCellWithReuseIdentifier:kActionCellReuseIdentifier forIndexPath:indexPath];
        actionCell.textLabel.text = action.title;
        if ( action.style == PickerActionStyleDefault ) {
            actionCell.textLabel.textColor = [UIColor blackColor];
        }
        else if ( action.style == PickerActionStyleSpecial ) {
            actionCell.textLabel.textColor = [UIColor flamingoColor];
        }
        else {
            actionCell.textLabel.textColor = [UIColor flamingoColor];
        }
        
        actionCell.normalBackgroundColor      = [UIColor colorWithWhite:0.97 alpha:1];
        actionCell.highlightedBackgroundColor = [UIColor colorWithWhite:0.92 alpha:1];
        
        cell = actionCell;
    }
    
    cell.backgroundInsets = [self.dataModel collectionView:collectionView 
                                  insetsForItemAtIndexPath:indexPath 
                                           cancelIndexPath:self.cancelIndexPath];
    cell.cornerStyle = [self.dataModel collectionView:collectionView 
                            cornersForItemAtIndexPath:indexPath 
                                      cancelIndexPath:self.cancelIndexPath];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath.section == 1 ) {
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        
        if ( [self.delegate respondsToSelector:@selector(actionSelected:)] ) {
            ImagePickerAction * action = self.dataSource.actions[indexPath.row];
            [self.delegate actionSelected:action];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {    
    return [self.dataModel collectionView:collectionView sizeForItemAtIndexPath:indexPath cancelIndexPath:self.cancelIndexPath];
}

- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section != 0;
}

@end
