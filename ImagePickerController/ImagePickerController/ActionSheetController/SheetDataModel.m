//
//  SheetDataModel.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "SheetDataModel.h"
@import Photos;

@implementation SheetDataModel

- (CGSize) collectionView:(UICollectionView*)collectionView 
   sizeForItemAtIndexPath:(NSIndexPath*)indexPath 
          cancelIndexPath:(NSIndexPath*)cancelIndexPath {
    
    CGFloat height = kActionHeight;
    
    if ( indexPath.section != 0 ) {
        CGFloat actionHeight = 50;
        
        UIEdgeInsets insets = [self collectionView:collectionView 
                          insetsForItemAtIndexPath:indexPath 
                                   cancelIndexPath:cancelIndexPath];
        height = actionHeight + insets.top + insets.bottom;
    }
    else {
        PHAuthorizationStatus authorizationStatus = [PHPhotoLibrary authorizationStatus];
        if( authorizationStatus == PHAuthorizationStatusAuthorized ) {
            height = kPreviewHeight;
        } 
        else {
            height = 0;
        }
    }
    
    return CGSizeMake( CGRectGetWidth(collectionView.bounds), height);
}

- (NSMutableArray*) allIndexPathsOfCollectionView:(UICollectionView*)collectionView  {
    NSMutableArray * indexPaths = [NSMutableArray new];
    for ( NSInteger section = 0; section < [collectionView.dataSource numberOfSectionsInCollectionView:collectionView]; section++ ) {
        for ( NSInteger row = 0; row < [collectionView.dataSource collectionView:collectionView numberOfItemsInSection:section]; row++ ) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
        }
    }
    return indexPaths;
}

- (UIEdgeInsets) collectionView:(UICollectionView*)collectionView 
       insetsForItemAtIndexPath:(NSIndexPath*)indexPath 
                cancelIndexPath:(NSIndexPath*)cancelIndexPath {
    
    NSMutableArray* indexPaths = [self allIndexPathsOfCollectionView:collectionView];
    CGFloat innerInset = 4;
    
    if ( [indexPath compare:indexPaths.firstObject] == NSOrderedSame ) {
        return UIEdgeInsetsMake( 0, defaultInset, 0, defaultInset);
    }
        
    if ( cancelIndexPath ) {
        if ( [cancelIndexPath compare:indexPath] == NSOrderedSame ) {
            return UIEdgeInsetsMake( innerInset, defaultInset, defaultInset, defaultInset);
        }
        
        [indexPaths removeLastObject];
        
        if ( [indexPath compare:indexPaths.lastObject] == NSOrderedSame ) {
            return UIEdgeInsetsMake( 0, defaultInset, innerInset, defaultInset);
        }
    }
    else if ( [indexPath compare:indexPaths.lastObject] == NSOrderedSame ) {
        return UIEdgeInsetsMake( 0, defaultInset, defaultInset, defaultInset);
    }
    
    return UIEdgeInsetsMake( 0, defaultInset, 0, defaultInset);
}

- (RoundedCorners) collectionView:(UICollectionView*)collectionView 
        cornersForItemAtIndexPath:(NSIndexPath*)indexPath 
                  cancelIndexPath:(NSIndexPath*)cancelIndexPath {
    
    NSMutableArray* indexPaths = [self allIndexPathsOfCollectionView:collectionView];
    
    if ( [indexPath compare:indexPaths.firstObject] == NSOrderedSame ) {
        return RoundedCornersTop;
    }
    
    if ( cancelIndexPath ) {
        if ( [cancelIndexPath compare:indexPath] == NSOrderedSame ) {
            return RoundedCornersAll;
        }
        
        [indexPaths removeLastObject];
        
        if ( [indexPath compare:indexPaths.lastObject] == NSOrderedSame ) {
            return RoundedCornersBottom;
        }
    }
    else if ( [indexPath compare:indexPaths.lastObject] == NSOrderedSame ) {
        return RoundedCornersBottom;
    }
    
    return RoundedCornersNone;
}

- (CGFloat) preferredHeightOfCollectionView:(UICollectionView*)collectionView cancelIndexPath:(NSIndexPath*)cancelIndexPath {
    NSMutableArray* indexPaths = [self allIndexPathsOfCollectionView:collectionView];
    CGFloat height = 0;
    
    for ( NSIndexPath * indexPath in indexPaths ) {
        height += [self collectionView:collectionView sizeForItemAtIndexPath:indexPath cancelIndexPath:cancelIndexPath].height;    
    }
    
    return height;
}

- (CGFloat) preferredWidthOfCollectionView:(UICollectionView*)collectionView { 
    return CGRectGetWidth( collectionView.bounds );
}


@end
