//
//  ActionSheetController.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ActionSheetController.h"
#import "ActionSheetController+CollectionView.h"
#import "ActionSheetCollectionLayout.h"

@interface ActionSheetController ()

@property (nonatomic, readwrite, strong) UICollectionView * sheetCollectionView;
@property (nonatomic, readwrite, strong) ImagesPreviewController * imagePreviewController;

@end

@implementation ActionSheetController

- (instancetype) init {
    self = [super init];
    
    if ( self ) {
        [self initialize];
    }
    
    return self;
}

- (void) initialize {
    [self setupSheetCollectionView];
    
    self.dataModel = [SheetDataModel new];    
    
    self.imagePreviewController = [[ImagesPreviewController alloc] init];
}

- (void) setupSheetCollectionView {
    ActionSheetCollectionLayout * layout = [ActionSheetCollectionLayout new];
    
    self.sheetCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 320) collectionViewLayout:layout];
    self.sheetCollectionView.dataSource                   = self;
    self.sheetCollectionView.delegate                     = self;
    self.sheetCollectionView.accessibilityIdentifier      = @"ActionSheetCollectionView";
    self.sheetCollectionView.backgroundColor              = [UIColor clearColor];
    self.sheetCollectionView.alwaysBounceVertical         = NO;
    self.sheetCollectionView.bounces                      = NO;
    self.sheetCollectionView.showsVerticalScrollIndicator = NO;
    self.sheetCollectionView.delaysContentTouches         = YES;
    
    [self.sheetCollectionView registerClass:[PreviewCell class] forCellWithReuseIdentifier:kPreviewCellReuseIdentifier];
    [self.sheetCollectionView registerClass:[ActionSheetCell class] forCellWithReuseIdentifier:kActionCellReuseIdentifier];
}

- (NSIndexPath*)cancelIndexPath {
    if ( !_cancelIndexPath ) {
        for ( NSInteger i = 0; i < self.dataSource.actions.count; i++ ) {
            ImagePickerAction* action = self.dataSource.actions[i];
            if ( action.style == PickerActionStyleCancel ) {
                _cancelIndexPath = [NSIndexPath indexPathForRow:i inSection:1];
            }
        }
    }
    return _cancelIndexPath;
}

- (CGFloat) preferredSheetWidth {
    return [self.dataModel preferredWidthOfCollectionView:self.sheetCollectionView];
}

- (CGFloat) preferredSheetHeight {
    return [self.dataModel preferredHeightOfCollectionView:self.sheetCollectionView cancelIndexPath:self.cancelIndexPath];
}

@end
