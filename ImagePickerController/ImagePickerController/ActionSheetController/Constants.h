//
//  Contants.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#ifndef Contants_h
#define Contants_h

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

static NSString * const kActionCellReuseIdentifier  = @"ActionCellReuseIdentifier";
static NSString * const kPreviewCellReuseIdentifier = @"PreviewCellReuseIdentifier";
static NSString * const kImagePreviewCellReuseIdentifier = @"ImagePreviewCellReuseIdentifier";

static NSString * const kSelectedMediaItemsCount = @"selectedMediaItemsCount";

static NSString * const kMediaItemsSelectionChanged = @"mediaItemsSelectionChanged";

static NSString * const kSendMediaItems = @"sendMediaItems";

static const CGFloat kPreviewHeight             = 130;
static const CGFloat kActionHeight              = 55;
static const CGFloat defaultInset               = 10;
static const CGFloat previewCollectionViewInset = 5;

static const NSUInteger fetchLimit              = 20;
static const CGFloat previewSupplementaryInset  = 5;

typedef NS_ENUM(NSInteger, RoundedCorners) {
    RoundedCornersNone,
    RoundedCornersTop,
    RoundedCornersBottom,
    RoundedCornersAll
};

#endif /* Contants_h */
