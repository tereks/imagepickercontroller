//
//  ActionSheetCell.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "BaseCollectionCell.h"

@interface ActionSheetCell : BaseCollectionCell

@property (nonatomic, strong) UILabel * textLabel;
@property (nonatomic, strong) UIColor * highlightedBackgroundColor;
@property (nonatomic, strong) UIColor * normalBackgroundColor;

@end
