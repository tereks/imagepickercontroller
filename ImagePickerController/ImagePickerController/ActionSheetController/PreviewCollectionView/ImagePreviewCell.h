//
//  ImagePreviewCell.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreviewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UIImageView * videoIndicatorView;

@end
