//
//  ImageSupplementaryView.h
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSupplementaryView : UICollectionReusableView

@property (nonatomic, assign) UIEdgeInsets buttonInset;
@property (nonatomic, assign) BOOL selected;

+ (UIImage*) selectedImage;
+ (UIImage*) normalImage;

@end
