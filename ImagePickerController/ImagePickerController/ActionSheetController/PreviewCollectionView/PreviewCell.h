//
//  PreviewCell.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "BaseCollectionCell.h"

@interface PreviewCell : BaseCollectionCell

@property (nonatomic, strong) UICollectionView * collectionView;

@end
