//
//  ImagesPreviewController.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIkit.h>

#import "Constants.h"
#import "ImagesPreviewDataSource.h"
#import "ImagePreviewCollectionLayout.h"

@interface ImagesPreviewController : NSObject

@property (nonatomic, readonly, strong) UICollectionView * previewCollectionView;
@property (nonatomic, readonly, strong) ImagePreviewCollectionLayout * imagesCollectionLayout;

@property (nonatomic, assign) ImagesPreviewDataSource * dataSource;

@property (nonatomic, strong) NSMutableArray * selectedImageIndexes;
@property (nonatomic, strong) NSMutableDictionary * supplementaryViews;

- (void) selectionChanged;
- (void) getSelectedAssets:(void (^)(NSArray* assets))completion;

@end
