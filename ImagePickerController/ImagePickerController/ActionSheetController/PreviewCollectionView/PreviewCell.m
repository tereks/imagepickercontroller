//
//  PreviewCell.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "PreviewCell.h"

@interface PreviewCell ()

@end

@implementation PreviewCell

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
    }
    return self;
}

- (void) setCollectionView:(UICollectionView *)collectionView {
    if ( !_collectionView ) {
        [self.contentView addSubview:collectionView];
    }
    _collectionView = collectionView;    
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    _collectionView.frame = CGRectMake( defaultInset, defaultInset, CGRectGetWidth(self.contentView.frame)- 2*defaultInset, CGRectGetHeight(self.contentView.frame) - 2*defaultInset);
    
    CGFloat lineHeight   = 1.f / [UIScreen mainScreen].scale;
    self.separatorView.frame = CGRectMake( 0, CGRectGetHeight(self.contentView.frame)-lineHeight,
                                          CGRectGetWidth(self.contentView.frame), lineHeight);
    
    [self reloadMask];
}

@end
