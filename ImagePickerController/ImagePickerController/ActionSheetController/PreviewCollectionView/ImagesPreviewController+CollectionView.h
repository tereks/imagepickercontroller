//
//  ImagesPreviewController+CollectionView.h
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagesPreviewController.h"

@interface ImagesPreviewController (CollectionView) <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end
