//
//  ImageSupplementaryView.m
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImageSupplementaryView.h"

@interface ImageSupplementaryView ()

@property (nonatomic, strong) UIButton * checkButton;

@end

@implementation ImageSupplementaryView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {        
        self.checkButton = [[UIButton alloc] init];
        self.checkButton.tintColor = [UIColor whiteColor];
        self.checkButton.userInteractionEnabled = NO;
        [self.checkButton setImage:[ImageSupplementaryView selectedImage] forState:UIControlStateNormal];
        [self.checkButton setImage:[ImageSupplementaryView selectedImage] forState:UIControlStateSelected];
        
        [self addSubview:self.checkButton];
        
        self.tintColor = [UIColor orangeColor];
        _selected = NO;
    }
    return self;
}

+ (UIImage*) selectedImage {
    return [UIImage imageNamed:@"checkSelected"];
}

+ (UIImage*) normalImage {
    return [UIImage imageNamed:@"checkNormal"];
}

- (void) setSelected:(BOOL)selected {
    _selected = selected;
    
    self.checkButton.selected = selected;
    [self reloadButtonBackgroundColor];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.selected = NO;
}

- (void) tintColorDidChange {
    [super tintColorDidChange];
    
    [self reloadButtonBackgroundColor];
}

- (void) reloadButtonBackgroundColor {
    self.checkButton.backgroundColor = self.selected ? self.tintColor : nil;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    [self.checkButton sizeToFit];
    
    self.checkButton.frame = CGRectMake( self.buttonInset.left, CGRectGetHeight(self.bounds)-CGRectGetHeight(self.checkButton.frame)-self.buttonInset.bottom, CGRectGetWidth(self.checkButton.frame), CGRectGetHeight(self.checkButton.frame));
    self.checkButton.layer.cornerRadius = CGRectGetHeight(self.checkButton.frame) / 2.0;
}

@end
