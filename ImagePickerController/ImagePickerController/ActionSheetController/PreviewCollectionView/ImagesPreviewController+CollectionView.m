//
//  ImagesPreviewController+CollectionView.m
//  ImagePickerController
//
//  Created by Sergey Kim on 22.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagesPreviewController+CollectionView.h"

#import "ImagePreviewCell.h"
#import "ImageSupplementaryView.h"

@implementation ImagesPreviewController (CollectionView)

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataSource.assets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell;
    
    ImagePreviewCell * previewCell = [collectionView 
                                      dequeueReusableCellWithReuseIdentifier:kImagePreviewCellReuseIdentifier 
                                      forIndexPath:indexPath];
    
    PHAsset * asset = self.dataSource.assets[indexPath.section];
    previewCell.videoIndicatorView.hidden = asset.mediaType != PHAssetMediaTypeVideo;
    
    [ImagesPreviewDataSource requestImageForAsset:asset completion:^(UIImage *image) {
        previewCell.imageView.image = image;
    }];
    
    cell.selected = [self.selectedImageIndexes containsObject:@(indexPath.section)];
    
    cell = previewCell;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    ImageSupplementaryView* view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([ImageSupplementaryView class]) forIndexPath:indexPath];  
                                    
    view.userInteractionEnabled = NO;
    view.buttonInset = UIEdgeInsetsMake(0.0, previewSupplementaryInset, previewSupplementaryInset, 0.0);
    view.selected = [self.selectedImageIndexes containsObject:@(indexPath.section)];
    
    [self.supplementaryViews setObject:view forKey:@(indexPath.section)];
    
    return view;    
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( ![self.selectedImageIndexes containsObject:@(indexPath.section)] ) {
        [self.selectedImageIndexes addObject:@(indexPath.section)];
    }
    
    [self selectionChanged];
    
    self.imagesCollectionLayout.showsSupplementaryViews = YES;
    
    ImageSupplementaryView* view = [self.supplementaryViews objectForKey:@(indexPath.section)];
    view.selected = YES;
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( [self.selectedImageIndexes containsObject:@(indexPath.section)] ) {
        [self.selectedImageIndexes removeObject:@(indexPath.section)];
    }
    
    [self selectionChanged];
    
    ImageSupplementaryView* view = [self.supplementaryViews objectForKey:@(indexPath.section)];
    view.selected = NO;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PHAsset * asset = self.dataSource.assets[indexPath.section];
    CGSize size = [ImagesPreviewDataSource sizeForAsset:asset scale:1];
    
    CGFloat currentImagePreviewHeight = CGRectGetHeight(collectionView.frame) - 2*previewCollectionViewInset;
    CGFloat scale = currentImagePreviewHeight / size.height;
    
    return CGSizeMake(size.width * scale, currentImagePreviewHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView 
                  layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGFloat checkmarkWidth = [ImageSupplementaryView normalImage].size.width;
    return CGSizeMake(checkmarkWidth + 2 * previewSupplementaryInset, CGRectGetHeight(collectionView.frame) - 2 * previewSupplementaryInset);
}

@end
