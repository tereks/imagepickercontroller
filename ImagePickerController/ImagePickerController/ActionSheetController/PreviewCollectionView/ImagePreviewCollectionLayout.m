//
//  ImagePreviewCollectionLayout.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePreviewCollectionLayout.h"
#import "Constants.h"

@interface ImagePreviewCollectionLayout ()

@property (nonatomic, strong) NSIndexPath * invalidationCenteredIndexPath;

@property (nonatomic, strong) NSMutableArray<UICollectionViewLayoutAttributes*>* layoutAttributes;
@property (nonatomic, assign) CGSize contentSize;

@end

@implementation ImagePreviewCollectionLayout

- (void) setShowsSupplementaryViews:(BOOL)showsSupplementaryViews {
    _showsSupplementaryViews = showsSupplementaryViews;
    [self invalidateLayout];
}

- (instancetype) init {
    self = [super init];
    
    if ( self ) {
        self.layoutAttributes            = [NSMutableArray new];
        self.contentSize                 = CGSizeZero;
        
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        self.sectionInset = UIEdgeInsetsMake(previewCollectionViewInset, previewCollectionViewInset, previewCollectionViewInset, previewCollectionViewInset);
        _showsSupplementaryViews = NO;
    }
    
    return self;
}

- (void) prepareLayout {
    [super prepareLayout];
    
    [self.layoutAttributes removeAllObjects];
    self.contentSize = CGSizeZero;
    
    if ( self.collectionView && self.collectionView.dataSource && self.collectionView.delegate ) {
        
        CGPoint origin = CGPointMake( self.sectionInset.left, self.sectionInset.top);
        NSInteger numberOfSections = [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView];
        
        for ( NSInteger s = 0; s < numberOfSections; s++ ) {
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:0 inSection:s];
            
            id <UICollectionViewDelegateFlowLayout> delegate = (id <UICollectionViewDelegateFlowLayout>)self.collectionView.delegate;            
            CGSize size = [delegate collectionView:self.collectionView 
                                            layout:self 
                            sizeForItemAtIndexPath:indexPath];
            
            UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes 
                                                            layoutAttributesForCellWithIndexPath:indexPath];
            attributes.frame = (CGRect){ origin, size};
            attributes.zIndex = 0;
                        
            [self.layoutAttributes addObject:attributes];
            
            origin.x = CGRectGetMaxX(attributes.frame) + self.sectionInset.right;
        }
        
        self.contentSize = CGSizeMake( origin.x, CGRectGetHeight(self.collectionView.frame) );
    }
}

- (BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (CGSize) collectionViewContentSize {
    return self.contentSize;
}

- (CGPoint) targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset {
    CGPoint contentOffset = proposedContentOffset;
    if ( self.invalidationCenteredIndexPath ) {
        if ( self.collectionView ) {
            CGRect frame = self.layoutAttributes[self.invalidationCenteredIndexPath.section].frame;
            contentOffset.x = CGRectGetMidX(frame) - self.collectionView.frame.size.width / 2.0;
            
            contentOffset.x = MAX(contentOffset.x, -self.collectionView.contentInset.left);
            contentOffset.x = MIN(contentOffset.x, 
                                 [self collectionViewContentSize].width - CGRectGetWidth( self.collectionView.frame ) + self.collectionView.contentInset.right);
        }
        self.invalidationCenteredIndexPath = nil;
    }
    
    return [super targetContentOffsetForProposedContentOffset:contentOffset];
}

- (nullable NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {

    NSPredicate *intersects = [NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes* obj, 
                                                                    NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, obj.frame);
    }];
    NSArray * longArray = [self.layoutAttributes filteredArrayUsingPredicate:intersects];

    NSMutableArray * ret = [NSMutableArray new];
    for ( UICollectionViewLayoutAttributes * attributes in longArray) {
        UICollectionViewLayoutAttributes * supplementaryAttributes = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:attributes.indexPath];
        
        [ret addObject:attributes];
        if ( supplementaryAttributes ) {
            [ret addObject:supplementaryAttributes];
        }
    }

    return ret;
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    return self.layoutAttributes[itemIndexPath.section];
}

- (UICollectionViewLayoutAttributes *) layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind 
                                                                      atIndexPath:(NSIndexPath *)indexPath {
    
    if ( self.collectionView && self.collectionView.delegate ) {
        UICollectionViewLayoutAttributes * itemAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        if ( itemAttributes ) {
            UIEdgeInsets inset = self.collectionView.contentInset;
            CGRect bounds = self.collectionView.bounds;
            CGPoint contentOffset = [self _contentOffsetForInset:inset];
            
            CGSize visibleSize = bounds.size;
            visibleSize.width -= (inset.left+inset.right);
            
            CGRect visibleFrame = (CGRect){ contentOffset, visibleSize};
            
            id <UICollectionViewDelegateFlowLayout> delegate = (id <UICollectionViewDelegateFlowLayout>)self.collectionView.delegate;
            
            CGSize size = [delegate collectionView:self.collectionView layout:self referenceSizeForHeaderInSection:indexPath.section];
            
            CGFloat originX = MAX( CGRectGetMinX( itemAttributes.frame ), MIN(CGRectGetMaxX( itemAttributes.frame ) - size.width, CGRectGetMaxX(visibleFrame) - size.width));
            
            UICollectionViewLayoutAttributes* attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:elementKind withIndexPath:indexPath];
            attributes.zIndex = 1;
            attributes.hidden = NO;
            attributes.frame = (CGRect){ CGPointMake( originX, CGRectGetMinY( itemAttributes.frame)), size};
            
            return attributes;
        }
    }
    
    return nil;
}

- (CGPoint) _contentOffsetForInset:(UIEdgeInsets)inset {
    CGPoint contentOffset = self.collectionView.contentOffset;
    contentOffset.x += inset.left;
    contentOffset.y += inset.top;
    
    return contentOffset;
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
}

@end

