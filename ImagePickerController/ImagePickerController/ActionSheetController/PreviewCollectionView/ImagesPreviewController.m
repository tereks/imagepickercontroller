//
//  ImagesPreviewController.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagesPreviewController.h"

#import "ImagesPreviewController+CollectionView.h"
#import "ImagePreviewCell.h"
#import "ImageSupplementaryView.h"

@import Photos;

@interface ImagesPreviewController () 

@property (nonatomic, readwrite, strong) UICollectionView * previewCollectionView;
@property (nonatomic, readwrite, strong)  ImagePreviewCollectionLayout * imagesCollectionLayout;

@end

@implementation ImagesPreviewController

- (instancetype) init {
    self = [super init];
    
    if ( self ) {
        [self initialize];
    }
    
    return self;
}

- (void) initialize {
    [self setupPreviewCollectionView];
    
    _selectedImageIndexes = [NSMutableArray new]; 
    _supplementaryViews = [[NSMutableDictionary alloc] init];
}

- (void) setupPreviewCollectionView {
    _imagesCollectionLayout = [ImagePreviewCollectionLayout new];
    
    self.previewCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 320) collectionViewLayout:_imagesCollectionLayout];
    self.previewCollectionView.dataSource                     = self;
    self.previewCollectionView.delegate                       = self;
    self.previewCollectionView.accessibilityIdentifier        = @"PreviewCollectionView";
    self.previewCollectionView.backgroundColor                = [UIColor clearColor];
    self.previewCollectionView.alwaysBounceHorizontal         = NO;
    self.previewCollectionView.bounces                        = NO;
    self.previewCollectionView.showsVerticalScrollIndicator   = NO;
    self.previewCollectionView.showsHorizontalScrollIndicator = NO;
    self.previewCollectionView.delaysContentTouches           = YES;
    self.previewCollectionView.allowsMultipleSelection        = YES;
    
    [self.previewCollectionView registerClass:[ImagePreviewCell class] 
                   forCellWithReuseIdentifier:kImagePreviewCellReuseIdentifier];
    
    [self.previewCollectionView registerClass:[ImageSupplementaryView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([ImageSupplementaryView class])];
}

- (void) selectionChanged {
    
    NSDictionary * passData = @{ kSelectedMediaItemsCount : @(self.selectedImageIndexes.count) };
    [[NSNotificationCenter defaultCenter] postNotificationName:kMediaItemsSelectionChanged object:self userInfo:passData];
}


- (void) getSelectedAssets:(void (^)(NSArray* assets))completion {
    NSMutableArray * assets = [NSMutableArray array];
    
    dispatch_group_t group = dispatch_group_create();
    
    for ( NSNumber * section in self.selectedImageIndexes ) {
        dispatch_group_enter(group);
        
        PHAsset * asset = self.dataSource.assets[section.integerValue];
        [ImagesPreviewDataSource requestDataForAsset:asset completion:^(NSData *data) {
            if ( data ) {
                [assets addObject:data];
            }
            dispatch_group_leave(group);
        }];  
    }
                             
    dispatch_group_notify( group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        completion( [assets copy] );
    });
}

@end