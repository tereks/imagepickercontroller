//
//  ImagePreviewCell.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePreviewCell.h"

@implementation ImagePreviewCell

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
        _imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode     = UIViewContentModeScaleAspectFill;        
        [self.contentView addSubview:_imageView];
        
        _videoIndicatorView = [[UIImageView alloc] init];
        _videoIndicatorView.backgroundColor = [UIColor clearColor];
        _videoIndicatorView.contentMode     = UIViewContentModeScaleAspectFit;
        _videoIndicatorView.hidden          = YES;
        _videoIndicatorView.image           = [self videoImage];
        [self.contentView addSubview:_videoIndicatorView];
    }
    return self;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = self.contentView.frame;
    
    CGSize videoIndicatViewSize = _videoIndicatorView.image.size;
    CGFloat inset = 10;
    CGPoint videoIndicatorViewOrigin = CGPointMake( CGRectGetMinX( self.bounds ) + inset/2, 
                                                    CGRectGetMaxY( self.bounds ) - inset - videoIndicatViewSize.height);
    _videoIndicatorView.frame = (CGRect){ videoIndicatorViewOrigin, videoIndicatViewSize };
}

- (UIImage*) videoImage {
    return [UIImage imageNamed:@"videoIndicator"];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    _imageView.image = nil;
    _videoIndicatorView.hidden = true;
}


@end
