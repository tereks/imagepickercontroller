//
//  ImagePreviewCollectionLayout.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreviewCollectionLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) BOOL showsSupplementaryViews;

@end
