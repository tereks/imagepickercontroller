//
//  SheetDataModel.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIkit.h>
#import "Constants.h"

@interface SheetDataModel : NSObject

- (CGSize) collectionView:(UICollectionView*)collectionView 
   sizeForItemAtIndexPath:(NSIndexPath*)indexPath
          cancelIndexPath:(NSIndexPath*)cancelIndexPath;

- (NSMutableArray*) allIndexPathsOfCollectionView:(UICollectionView*)collectionView;

- (UIEdgeInsets) collectionView:(UICollectionView*)collectionView 
       insetsForItemAtIndexPath:(NSIndexPath*)indexPath 
                cancelIndexPath:(NSIndexPath*)cancelIndexPath;

- (RoundedCorners) collectionView:(UICollectionView*)collectionView 
        cornersForItemAtIndexPath:(NSIndexPath*)indexPath 
                  cancelIndexPath:(NSIndexPath*)cancelIndexPath;

- (CGFloat) preferredWidthOfCollectionView:(UICollectionView*)collectionView;
- (CGFloat) preferredHeightOfCollectionView:(UICollectionView*)collectionView cancelIndexPath:(NSIndexPath*)cancelIndexPath;

@end
