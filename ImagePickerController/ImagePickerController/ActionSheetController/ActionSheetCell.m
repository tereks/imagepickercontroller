//
//  ActionSheetCell.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ActionSheetCell.h"

@interface ActionSheetCell ()

@end

@implementation ActionSheetCell

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
        [self initialize];
    }
    return self;
}

- (void) initialize {
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.textLabel];  
}

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    [self reloadBackgroundColor];
}

- (void) setHighlightedBackgroundColor:(UIColor *)highlightedBackgroundColor {
    _highlightedBackgroundColor = highlightedBackgroundColor;
    
    [self reloadBackgroundColor];
}

- (void) setNormalBackgroundColor:(UIColor *)normalBackgroundColor {
    _normalBackgroundColor = normalBackgroundColor;
    
    [self reloadBackgroundColor];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.textLabel.frame = self.bounds;
    CGFloat lineHeight   = 1.f / [UIScreen mainScreen].scale;
    self.separatorView.frame = CGRectMake( 0, CGRectGetHeight(self.contentView.frame)-lineHeight,
                                          CGRectGetWidth(self.contentView.frame), lineHeight);
    [self reloadMask];
}

- (void) reloadBackgroundColor {
    self.backgroundColor = self.highlighted ? self.highlightedBackgroundColor : self.normalBackgroundColor;
}

@end
