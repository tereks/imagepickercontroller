//
//  ImagePickerAction.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PickerActionStyle) {
    PickerActionStyleDefault,
    PickerActionStyleSpecial,
    PickerActionStyleCancel
};

typedef void (^ActionHandler) ();

@interface ImagePickerAction : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, assign) PickerActionStyle style;
@property (nonatomic, copy) ActionHandler handler;

- (instancetype) initWithTitle:(NSString*)title style:(PickerActionStyle)style handler:(ActionHandler)handler;

@end
