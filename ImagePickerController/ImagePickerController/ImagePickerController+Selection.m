//
//  ImagePickerController+Selection.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePickerController+Selection.h"

@implementation ImagePickerController (Selection)

- (void) actionSelected:(ImagePickerAction*)action {
    if ( action.handler ) {
        action.handler();
    }
}

@end
