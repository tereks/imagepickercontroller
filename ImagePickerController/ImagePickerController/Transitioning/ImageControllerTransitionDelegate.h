//
//  ImageControllerTransitionDelegate.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageControllerTransitionDelegate : NSObject <UIViewControllerTransitioningDelegate>

@end
