//
//  ImageControllerAnimator.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImageControllerAnimator.h"
#import "ImagePickerController.h"

@implementation ImageControllerAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController* destination = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if([destination isBeingPresented]) {
        [self animatePresentation:transitionContext];
    } else {
        [self animateDismissal:transitionContext];
    }
}

- (void)animatePresentation:(id<UIViewControllerContextTransitioning>)transitionContext
{
    NSTimeInterval transitionDuration            = [self transitionDuration:transitionContext];
    UIViewController* sourceController           = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    ImagePickerController* destinationController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* container                            = transitionContext.containerView;
    
    sourceController.view.frame      = container.bounds;
    destinationController.view.frame = container.bounds;
    
    destinationController.actionsCollectionView.frame = CGRectOffset(destinationController.actionsCollectionView.frame, 0, 
                                                              CGRectGetHeight(destinationController.actionsCollectionView.frame));
        
    [container addSubview:destinationController.view];
    destinationController.view.alpha = 0;
    
    [UIView animateKeyframesWithDuration:transitionDuration delay:0.0
                                 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
                                     destinationController.view.alpha = 1; 
                                     destinationController.actionsCollectionView.frame = CGRectOffset(destinationController.actionsCollectionView.frame, 0, 
                                        -CGRectGetHeight(destinationController.actionsCollectionView.frame));

                                 } completion:^(BOOL finished) {
                                     [transitionContext completeTransition:YES];
                                 }];
}

- (void)animateDismissal:(id<UIViewControllerContextTransitioning>)transitionContext
{
    NSTimeInterval transitionDuration       = [self transitionDuration:transitionContext];
    ImagePickerController* sourceController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* destinationController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* container                       = transitionContext.containerView;
    
    destinationController.view.frame = container.bounds;
    sourceController.view.frame      = container.bounds;
    
    [UIView animateKeyframesWithDuration:transitionDuration delay:0.0
                                 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
                                     [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:1.0 animations:^{
                                         sourceController.actionsCollectionView.frame = CGRectOffset(sourceController.actionsCollectionView.frame, 0, 
                                             CGRectGetHeight(sourceController.actionsCollectionView.frame));
                                         sourceController.view.alpha = 0; 
                                     }];
                                 } completion:^(BOOL finished) {
                                     [sourceController.view removeFromSuperview];
                                     [transitionContext completeTransition:YES];
                                 }];
}

@end
