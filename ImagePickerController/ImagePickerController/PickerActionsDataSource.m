//
//  PickerActionsDataSource.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "PickerActionsDataSource.h"
#import "ImagePickerAction.h"
#import "TitleHelper.h"
#import "Constants.h"

#import <MobileCoreServices/UTCoreTypes.h>
#import <MobileCoreServices/UTType.h>
#import <AVFoundation/AVFoundation.h>

#import "QBImagePickerController.h"
#import "ImagesPreviewDataSource.h"

@interface PickerActionsDataSource () <UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate, QBImagePickerControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, readwrite, strong) NSMutableArray * defaultActions;
@property (nonatomic, readwrite, strong) NSMutableArray * mediaSelectedActions;

@property (nonatomic, strong) UIViewController * hostController;
@property (nonatomic, strong) ImagePickerAction * actionAttach;
@end

@implementation PickerActionsDataSource

- (instancetype) init {
    self = [super init];
    if ( self ) {
        [self setupLists];
    }
    return self;
}

- (instancetype) initWithController:(UIViewController*)controller {
    self = [super init];
    if ( self ) {
        _hostController = controller;
        [self setupLists];
    }
    return self;
}

- (void) setupLists {
    self.defaultActions       = [NSMutableArray new];
    self.mediaSelectedActions = [NSMutableArray new];
    
    ImagePickerAction * actionAlbums = [[ImagePickerAction alloc] initWithTitle:@"All albums" 
                                                                          style:PickerActionStyleDefault handler:^{
                                                                              [self openLibrary];
                                                                          }];
    
    ImagePickerAction * actionTakePhoto = [[ImagePickerAction alloc] initWithTitle:@"Take a picture" 
                                                                          style:PickerActionStyleDefault handler:^{
                                                                              [self openCamera];
                                                                          }];
    
    ImagePickerAction * actionOtherApps = [[ImagePickerAction alloc] initWithTitle:@"Other apps" 
                                                                          style:PickerActionStyleDefault handler:^{
                                                                              [self openOthers];
                                                                          }];
    
    ImagePickerAction * actionCancel = [[ImagePickerAction alloc] initWithTitle:@"Cancel" 
                                                                          style:PickerActionStyleCancel handler:^{
                                                                              [self dismissView];
                                                                          }];
    
    [self.defaultActions addObjectsFromArray:@[actionAlbums, actionTakePhoto, actionOtherApps, actionCancel]];
    
    self.actionAttach = [[ImagePickerAction alloc] initWithTitle:@"Attach files" 
                                                                          style:PickerActionStyleSpecial handler:^{
                                                                              [self attachFiles];
                                                                          }];
    
    [self.mediaSelectedActions addObjectsFromArray:@[self.actionAttach, actionTakePhoto, actionOtherApps, actionCancel]];
}

- (NSArray*) actions {
    if ( self.mediaSelected ) {
        return self.mediaSelectedActions;
    }
    return self.defaultActions;
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (void) openCamera {
    if ( _hostController ) {
        [_hostController dismissViewControllerAnimated:YES completion:^{
            
            if ( [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] != AVAuthorizationStatusAuthorized )
            {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Camera Unavailable"
                                             message:@"Cannot get access to camera on your device"
                                             preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                
                [self.topMostController presentViewController:alert animated:YES completion:nil]; 
                return;
            }
            
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePicker.allowsEditing = YES;
                imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                    
                [imagePicker setShowsCameraControls:YES];
                imagePicker.videoMaximumDuration = 60;
                
                [self.topMostController presentViewController:imagePicker animated:YES completion:nil];
            }
            else {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Camera Unavailable"
                                             message:@"Unable to find a camera on your device"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
               [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];  
                
                [self.topMostController presentViewController:alert animated:YES completion:nil]; 
            }
        }];
        
    }
}

- (void) openLibrary {
    if ( _hostController ) {
        [_hostController dismissViewControllerAnimated:YES completion:^{
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] ) {
                
                QBImagePickerController *imagePickerController = [QBImagePickerController new];
                imagePickerController.delegate = self;
                imagePickerController.allowsMultipleSelection = YES;
                imagePickerController.maximumNumberOfSelection = 10;
                imagePickerController.showsNumberOfSelectedAssets = YES;

                [self.topMostController presentViewController:imagePickerController animated:YES completion:nil];
            }
            else {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Library Unavailable"
                                             message:@"Unable to open photos on your device"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                
                [self.topMostController presentViewController:alert animated:YES completion:nil]; 
            }
        }];        
    }
}

- (void) dismissView {
    if ( _hostController ) {
        [_hostController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) openOthers {
    if ( _hostController ) {
        [_hostController dismissViewControllerAnimated:YES completion:^{
            UIDocumentMenuViewController *importMenu =
            [[UIDocumentMenuViewController alloc] initWithDocumentTypes:@[@"public.image"]
                                                                 inMode:UIDocumentPickerModeImport];
            
            importMenu.delegate = self;
            [self.topMostController presentViewController:importMenu animated:YES completion:nil];
        }];
    }
}

- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    
    documentPicker.delegate = self;
    [self.topMostController presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if ( controller.documentPickerMode != UIDocumentPickerModeImport ) {
        return;
    }
    
    NSString * absPath = [url absoluteString];
    CFStringRef fileExtension = (__bridge CFStringRef) [absPath pathExtension];
    CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
    
    __block NSArray * assets = nil;
    if ( UTTypeConformsTo(fileUTI, kUTTypeImage) || 
         UTTypeConformsTo(fileUTI, kUTTypeMovie) || 
         UTTypeConformsTo(fileUTI, kUTTypeVideo) ) {
        
        NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        NSError *error = nil;
        [coordinator coordinateReadingItemAtURL:url 
                                        options:0 
                                          error:&error 
                                     byAccessor:^(NSURL *newURL) {
                                         
                                         NSData *mediaData = [NSData dataWithContentsOfURL:newURL];
                                         if ( mediaData ) {
                                             assets = @[mediaData];
                                         }
        }];
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Import"
                                                      message:@"Import failed"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self.topMostController presentViewController:alertController animated:YES completion:nil];
            });
        }
    }
    
    CFRelease(fileUTI);    
    
    if ( assets ) {
        if ( [_delegate respondsToSelector:@selector(attachMediaItems:)] ) {
            [_delegate attachMediaItems:assets];
        }
    }
}

#pragma mark - ImagePickerController

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSArray * assets = nil;
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        if ( picker.sourceType == UIImagePickerControllerSourceTypeCamera ) {
            UIImageWriteToSavedPhotosAlbum(originalImage, nil, nil, nil);
        }
        
        NSData *imageData = UIImagePNGRepresentation(originalImage);
        if ( imageData ) { 
            assets = @[imageData];
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeVideo] || [mediaType isEqualToString:(NSString *)kUTTypeMovie] ) {
        NSURL *videoUrl = (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];
        
        if ( picker.sourceType == UIImagePickerControllerSourceTypeCamera ) {
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
            }
        }
        
        NSData *videoData = [NSData dataWithContentsOfURL:videoUrl];
        if ( videoData ) { 
            assets = @[videoData];
        }
    }

    if ( assets ) {
        if ( [_delegate respondsToSelector:@selector(attachMediaItems:)] ) {
            [_delegate attachMediaItems:assets];
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    
    if ( assets ) {
        NSMutableArray * dataArray = [NSMutableArray new];
        dispatch_group_t group = dispatch_group_create();
        
        for ( PHAsset *asset in assets ) {
            dispatch_group_enter(group);
            
            [ImagesPreviewDataSource requestDataForAsset:asset completion:^(NSData *data) {
                if ( data ) {
                    [dataArray addObject:data];
                }
                dispatch_group_leave(group);
            }];  
        }
        
        dispatch_group_notify( group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
            if ( [_delegate respondsToSelector:@selector(attachMediaItems:)] ) {
                [_delegate attachMediaItems:dataArray];
            }
        });       
        
    }
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}


- (void) setAssetsCount:(NSUInteger)assetsCount {
    _assetsCount = assetsCount;  
    
    self.actionAttach.title = [TitleHelper nameWithCount:_assetsCount];
}

- (void) attachFiles {
    if ( [_delegate respondsToSelector:@selector(attachMediaItems)] ) {
        [_delegate attachMediaItems];
    }
}
    
@end
