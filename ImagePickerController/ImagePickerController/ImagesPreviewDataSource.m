//
//  ImagesPreviewDataSource.m
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagesPreviewDataSource.h"
#import "Constants.h"

@interface ImagesPreviewDataSource ()

@property (nonatomic, readwrite, strong) NSArray * assets;
@property (nonatomic, strong) PHCachingImageManager * imageManager;

@end


@implementation ImagesPreviewDataSource

- (instancetype) init {
    self = [super init];
    if ( self ) {
    }
    return self;
}

- (void) fetchMediaItems {
    self.imageManager = [[PHCachingImageManager alloc] init];
    
    NSMutableArray * assets = [NSMutableArray new];
    
    PHFetchOptions* options = [[PHFetchOptions alloc] init];
    
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"creationDate" ascending:NO];
    options.sortDescriptors = @[descriptor];
    
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d OR mediaType = %d", 
                         PHAssetMediaTypeImage, PHAssetMediaTypeVideo];
    if ( SYSTEM_VERSION_GREATER_THAN(@"9.0") ) {
        options.fetchLimit = fetchLimit;
    }

    PHFetchResult * result = [PHAsset fetchAssetsWithOptions:options];
    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    
    requestOptions.synchronous  = YES;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    
    NSUInteger count = MIN( fetchLimit, result.count );
    for ( NSInteger idx = 0; idx < count; idx++ ) {
        PHAsset * pAsset = [result objectAtIndex:idx];
        
        [self.imageManager 
         requestImageDataForAsset:pAsset 
         options:requestOptions 
         resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             
             if ( imageData != nil ) {
                 [assets addObject:pAsset];
             }
         }];
    };
        
    self.assets = [assets copy];
}

+ (CGSize) sizeForAsset:(PHAsset*)asset scale:(CGFloat)scale {
    CGFloat proportion = (double)asset.pixelWidth / (double)asset.pixelHeight;
    
    CGFloat imageHeight = kPreviewHeight - 2 * previewCollectionViewInset;
    CGFloat imageWidth = floor(proportion * imageHeight);
    
    return CGSizeMake( imageWidth * scale, imageHeight * scale);
}

+ (void) requestImageForAsset:(PHAsset*)asset completion:(void (^)(UIImage* image))completion {
    CGSize targetSize = [self sizeForAsset:asset scale:UIScreen.mainScreen.scale];
    
    PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
    
    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.synchronous  = YES;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
    
    if ( asset.representsBurst ) {
        [imageManager 
         requestImageDataForAsset:asset 
         options:requestOptions 
         resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
             UIImage * image = nil;
             if ( imageData ) {
                 image = [UIImage imageWithData:imageData];
             }
             completion(image);
         }];
    }
    else {
        [imageManager 
        requestImageForAsset:asset targetSize:targetSize contentMode:PHImageContentModeAspectFill options:requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            completion(result);
        }];
    }
}

+ (void) requestDataForAsset:(PHAsset*)asset completion:(void (^)(NSData* data))completion {
    
    PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
    
    if ( asset.mediaType == PHAssetMediaTypeVideo ) {
        PHVideoRequestOptions * requestOptions = [[PHVideoRequestOptions alloc] init];
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        
        [imageManager 
         requestAVAssetForVideo:asset 
         options:requestOptions 
         resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
             AVURLAsset * urlAsset = (AVURLAsset *)asset;
             NSData *data = [NSData dataWithContentsOfURL:urlAsset.URL];
             
             completion(data);
         }];  
    }
    else {
        CGSize targetSize = [self sizeForAsset:asset scale:UIScreen.mainScreen.scale];
        
        PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
        requestOptions.synchronous  = YES;
        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
        
        if ( asset.representsBurst ) {
            [imageManager 
             requestImageDataForAsset:asset 
             options:requestOptions 
             resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                 
                 completion(imageData);
             }];
        }
        else {
            [imageManager 
             requestImageForAsset:asset targetSize:targetSize contentMode:PHImageContentModeAspectFill options:requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                 
                 NSData *imageData = UIImagePNGRepresentation(result);
                 
                 completion(imageData);
             }];
        }
    }
}

@end
