//
//  PickerActionsDataSource.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickerActionsDelegate <NSObject>

@optional 
- (void) attachMediaItems;
- (void) attachMediaItems:(NSArray*)assets;

@end

@interface PickerActionsDataSource : NSObject

@property (nonatomic, readonly, strong) NSArray * actions;
@property (nonatomic, assign) BOOL mediaSelected;
@property (nonatomic, assign) NSUInteger assetsCount;
@property (nonatomic, weak) id<PickerActionsDelegate> delegate;

- (instancetype) initWithController:(UIViewController*)controller;

@end
