//
//  BaseCollectionCell.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"

@interface BaseCollectionCell : UICollectionViewCell

@property (nonatomic, assign) RoundedCorners cornerStyle;
@property (nonatomic, assign) UIEdgeInsets backgroundInsets;

@property (nonatomic, assign) BOOL needMasking;
@property (nonatomic, strong) UIView* separatorView;

- (void) reloadMask;

@end
