//
//  BaseCollectionCell.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "BaseCollectionCell.h"

@implementation BaseCollectionCell

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( self ) {
        self.separatorView = [[UIView alloc] initWithFrame:CGRectZero];
        self.separatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
        [self.contentView addSubview:_separatorView];
        
        self.backgroundColor = [UIColor whiteColor]; 
    }
    return self;
}

- (BOOL) needsMasking {
    if ( !UIEdgeInsetsEqualToEdgeInsets(self.backgroundInsets, UIEdgeInsetsZero) ) {
        return YES;
    }
    
    switch ( self.cornerStyle ) {
        case RoundedCornersNone:
            return NO;
        default:
            return YES;
    }
}

- (void) reloadMask {
    if ( self.needsMasking && self.layer.mask == nil ) {
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.lineWidth = 0;
        maskLayer.fillColor = [UIColor blackColor].CGColor;
        
        self.layer.mask = maskLayer;
    }
    
    CAShapeLayer* layerMask = (CAShapeLayer*)self.layer.mask;
    layerMask.frame = self.bounds;
    layerMask.path = [self maskPathWithRect:UIEdgeInsetsInsetRect(self.bounds, self.backgroundInsets) roundedCorner:self.cornerStyle];
}

- (CGPathRef) maskPathWithRect:(CGRect)rect roundedCorner:(RoundedCorners)roundedCorner {
    CGSize radius = CGSizeMake(10.0, 10.0); 
    UIRectCorner corners;
    
    switch (roundedCorner) {
        case RoundedCornersAll:
            corners = UIRectCornerAllCorners;
            break;
        case RoundedCornersTop:
            corners = UIRectCornerTopLeft | UIRectCornerTopRight;
            break;
        case RoundedCornersBottom:
            corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            
            break;
        default:
            return [UIBezierPath bezierPathWithRect:rect].CGPath;
            break;
    }
    
    return [UIBezierPath bezierPathWithRoundedRect:rect
                                 byRoundingCorners:corners
                                       cornerRadii:radius].CGPath;
}

@end
