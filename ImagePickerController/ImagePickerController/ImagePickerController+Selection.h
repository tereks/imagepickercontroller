//
//  ImagePickerController+Selection.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePickerController.h"

@interface ImagePickerController (Selection) <ActionSheetControllerDelegate>

@end
