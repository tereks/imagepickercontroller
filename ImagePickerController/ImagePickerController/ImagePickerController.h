//
//  ImagePickerController.h
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ActionSheetController.h"

@protocol ImagePickerControllerDelegate <NSObject>

- (void) mediaAssetsSelected:(NSArray*)assets;

@end

@interface ImagePickerController : UIViewController

@property (nonatomic, strong) UICollectionView * actionsCollectionView;
@property (nonatomic, weak) id<ImagePickerControllerDelegate> delegate;

@end
