//
//  ImagePickerController.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePickerController.h"
#import "ImageControllerTransitionDelegate.h"

#import "ImagePickerController+Selection.h"
#import "PickerActionsDataSource.h"
#import "ImagesPreviewDataSource.h"

@interface ImagePickerController () <PickerActionsDelegate>

@property (nonatomic, strong) ImageControllerTransitionDelegate * transitionDelegate;
@property (nonatomic, strong) ActionSheetController   * sheetController;
@property (nonatomic, strong) ImagesPreviewDataSource * assetsDataSource;

@end

@implementation ImagePickerController

- (instancetype)init {
    self = [super init];
    if ( self ) {
        [self initialize];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if ( self ) {
        [self initialize];
    }
    return self;
}

- (void) initialize {
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitionDelegate = [ImageControllerTransitionDelegate new];
    self.transitioningDelegate = self.transitionDelegate; 
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(mediaSelectionChanged:) 
                                                 name:kMediaItemsSelectionChanged 
                                               object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self]; 
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [tapGesture setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapGesture];
        
    self.sheetController            = [ActionSheetController new];
    self.sheetController.delegate   = self;

    self.sheetController.dataSource = [[PickerActionsDataSource alloc] initWithController:self];
    self.sheetController.dataSource.delegate = self;
    self.actionsCollectionView      = _sheetController.sheetCollectionView;

    [self.view addSubview:self.actionsCollectionView]; 
    
    self.assetsDataSource = [ImagesPreviewDataSource new];
    [self.assetsDataSource fetchMediaItems];
    
    self.sheetController.imagePreviewController.dataSource = self.assetsDataSource;
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];   
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self layoutSheetCollectionView];    
}

- (void) layoutSheetCollectionView {
    CGFloat sheetHeight = self.sheetController.preferredSheetHeight;
    CGSize sheetSize = CGSizeMake( CGRectGetWidth( self.view.bounds ), sheetHeight);
    
    self.preferredContentSize = sheetSize;
    self.actionsCollectionView.frame = (CGRect) { CGPointMake( CGRectGetMinX( self.view.bounds ), CGRectGetMaxY (self.view.frame)-sheetHeight), sheetSize};
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) tapGesture:(UITapGestureRecognizer*)sender {
    CGPoint loc = [sender locationInView:sender.view];
    UIView* subview = [sender.view hitTest:loc withEvent:nil];
    
    if ( [self.view isEqual:subview] ) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) mediaSelectionChanged:(NSNotification*)notification {
    NSDictionary * dict = notification.userInfo;
    
    NSNumber * selectedItemsCount = dict[kSelectedMediaItemsCount];
    if ( selectedItemsCount ) {
        self.sheetController.dataSource.mediaSelected = selectedItemsCount.integerValue > 0;
        self.sheetController.dataSource.assetsCount    = selectedItemsCount.integerValue;
        [self.sheetController.sheetCollectionView reloadData];
    }
}

#pragma mark - PickerActionsDelegate
- (void) attachMediaItems:(NSArray*)assets {

    dispatch_async(dispatch_get_main_queue(), ^{
        [self proceedWithAssets:assets];
    });
}

- (void) attachMediaItems {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.sheetController.imagePreviewController getSelectedAssets:^(NSArray *assets) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self proceedWithAssets:assets];
        });
    }];
}

#pragma mark 
- (void) proceedWithAssets:(NSArray*)assets {
    if ( [_delegate respondsToSelector:@selector(mediaAssetsSelected:)] ) {
        [_delegate mediaAssetsSelected:assets];
    }
}

@end
