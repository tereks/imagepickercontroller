//
//  ImagePickerAction.m
//  ImagePickerController
//
//  Created by Sergey Kim on 18.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "ImagePickerAction.h"

@interface ImagePickerAction ()

@end

@implementation ImagePickerAction

- (instancetype) initWithTitle:(NSString*)title style:(PickerActionStyle)style handler:(ActionHandler)handler {
    
    self = [super init];
    if ( self ) {
        self.title   = title;
        self.style   = style;
        self.handler = handler;
    }
    return self;
}

@end
