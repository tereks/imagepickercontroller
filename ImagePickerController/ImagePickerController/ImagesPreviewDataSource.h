//
//  ImagesPreviewDataSource.h
//  ImagePickerController
//
//  Created by Sergey Kim on 21.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Photos;

@interface ImagesPreviewDataSource : NSObject

@property (nonatomic, readonly, strong) NSArray * assets;

- (void) fetchMediaItems;

+ (CGSize) sizeForAsset:(PHAsset*)asset scale:(CGFloat)scale;
+ (void) requestImageForAsset:(PHAsset*)asset completion:(void (^)(UIImage* image))completion; 
+ (void) requestDataForAsset:(PHAsset*)asset completion:(void (^)(NSData* data))completion;

@end
